﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alphabets_Recognition_Project.NeuralNetwork
{
    class ReLuConvolutionLayer : AConvolutionLayer
    {
        /// <summary>
        /// 활성화 함수가 ReLU인 Convolution Layer 생성자
        /// </summary>
        /// <param name="prevImgWidth">이전 레이어의 Padding을 포함한 이미지 가로 크기</param>
        /// <param name="prevImgHeight">이전 레이어의 Padding을 포함한 이미지 세로 크기</param>
        /// <param name="countOfChannels">이미지의 채널 수</param>
        /// <param name="filterWidth">Filter의 가로 크기</param>
        /// <param name="filterHeight">Filter의 세로 크기</param>
        /// <param name="countOfFilters">Filter의 수</param>
        /// <param name="padding">현재 레이어의 Padding</param>
        /// <param name="stride">Filter의 Stride</param>
        /// <param name="filterInitWay">Filter를 초기화하는 방법</param>
        /// <param name="filterValue">Filter의 초기값</param>
        /// <param name="biasValue">Bias의 초기값</param>
        public ReLuConvolutionLayer(int prevImgWidth, int prevImgHeight, int countOfChannels,
            int filterWidth, int filterHeight, int countOfFilters, int padding, int stride,
            EInitWays filterInitWay, double filterValue, double biasValue) 
            : base(prevImgWidth, prevImgHeight, countOfChannels, filterWidth, filterHeight, countOfFilters, padding, stride, filterInitWay, filterValue, biasValue)
        {

        }

        public override double Activate(double input)
        {
            if (input > 0.0) return input;
            return 0.0;
        }
        public override double DeltaActivate(double input)
        {
            if (input > 0.0) return 1.0;
            return 0.0;
        }
    }
}
