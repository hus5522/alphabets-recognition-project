﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alphabets_Recognition_Project
{
    class NeuralNet
    {
        public ALayer[] Layers { get { return mLayers; } }
        public int CountOfLayers { get { return mCountOfLayers; } }
        public double[] Inputs { get { return mLayers[0].Outputs; } }
        public double[] Outputs { get { return mLayers[mCountOfLayers - 1].Outputs; } }

        private ALayer[] mLayers;
        private int mCountOfLayers;

        public NeuralNet(int countOfLayers)
        {
            mCountOfLayers = countOfLayers;
            mLayers = new ALayer[mCountOfLayers];
        }

        public void Simulate()
        {
            for(int i=1; i<mCountOfLayers; i++)
            {
                mLayers[i].Simulate(mLayers[i - 1]);
            }
        }
    }
}
