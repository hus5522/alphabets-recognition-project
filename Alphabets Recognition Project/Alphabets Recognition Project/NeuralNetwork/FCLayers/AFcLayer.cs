﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alphabets_Recognition_Project.NeuralNetwork
{
    abstract class AFcLayer : ALayer
    {
        public AFcLayer(int countOfPrevUnits, int countOfCurUnits, EInitWays initWay, double weightValue, double biasValue) : base(countOfPrevUnits, countOfCurUnits)
        {
            Weights = new double[CountOfPrevUnits, CountOfCurUnits];
            Biases = new double[CountOfCurUnits];
            switch(initWay)
            {
                case EInitWays.RANDOM:
                    Random rand = new Random();
                    for (int i = 0; i < CountOfCurUnits; i++)
                    {
                        for (int j = 0; j < CountOfPrevUnits; j++)
                        {
                            Weights[j, i] = rand.NextDouble() * weightValue * 2.0 - weightValue;
                        }
                        Biases[i] = rand.NextDouble() * biasValue * 2.0 - biasValue;
                    }
                    break;
                case EInitWays.CONSTANT:
                    for (int i = 0; i < CountOfCurUnits; i++)
                    {
                        for (int j = 0; j < CountOfPrevUnits; j++)
                        {
                            Weights[j, i] = weightValue;
                        }
                        Biases[i] = biasValue;
                    }
                    break;
            }
        }

        public sealed override void Simulate(ALayer prevLayer)
        {
            double sum;

            for (int i = 0; i < CountOfCurUnits; i++)
            {
                sum = 0.0f;
                for (int j = 0; j < CountOfPrevUnits; j++)
                {
                    sum += prevLayer.Outputs[j] * Weights[j, i];
                }

                Totals[i] = sum + Biases[i];
                Outputs[i] = Activate(Totals[i]);
            }
        }
    }
}
