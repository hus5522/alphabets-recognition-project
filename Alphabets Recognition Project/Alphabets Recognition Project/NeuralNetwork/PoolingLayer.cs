﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alphabets_Recognition_Project.NeuralNetwork
{
    class PoolingLayer : ALayer
    {
        public int WindowWidth { get { return mWindowWidth; } }
        public int WindowHeight { get { return mWindowHeight; } }
        public int Stride { get { return mStride; } }
        public int PrevImgWidth { get { return mPrevImgWidth; } }
        public int PrevImgHeight { get { return mPrevImgHeight; } }
        public int CurImgWidth { get { return mCurImgWidth; } }
        public int CurImgHeight { get { return mCurImgHeight; } }
        public int CountOfChannels { get { return mCountOfChannels; } }

        // Pooling Window 가로 크기
        private int mWindowWidth;
        // Pooling Window 세로 크기
        private int mWindowHeight;
        // Pooling Window Stride
        private int mStride;
        // 이전 레이어 이미지 가로 크기
        private int mPrevImgWidth;
        // 이전 레이어 이미지 세로 크기
        private int mPrevImgHeight;
        // 현재 레이어 이미지 가로 크기
        private int mCurImgWidth;
        // 현재 레이어 이미지 세로 크기
        private int mCurImgHeight;
        // 이미지 채널 크기
        private int mCountOfChannels;

        public PoolingLayer(int prevImgWidth, int prevImgHeight, int countOfChannels, int windowWidth, int windowHeight, int stride)
            : base(prevImgWidth * prevImgHeight, ((prevImgWidth - windowWidth) / stride + 1) * ((prevImgHeight - windowHeight) / stride + 1) * countOfChannels)
        {
            Weights = new double[0, 0];
            Biases = new double[0];
            mPrevImgWidth = prevImgWidth;
            mPrevImgHeight = prevImgHeight;
            mWindowWidth = windowWidth;
            mWindowHeight = windowHeight;
            mStride = stride;
            mCountOfChannels = countOfChannels;

            mCurImgWidth = (mPrevImgWidth - mWindowWidth) / mStride + 1;
            mCurImgHeight = (mPrevImgHeight - mWindowHeight) / mStride + 1;
        }

        public override double Activate(double input)
        {
            return 0.0;
        }
        public override double DeltaActivate(double input)
        {
            return 0.0;
        }
        public override void Simulate(ALayer prevLayer)
        {
            double[] inputs = prevLayer.Outputs;
            double max;
            int indexOfMax;

            for (int channel = 0; channel < mCountOfChannels; channel++)
            {
                for (int prevRow = 0; prevRow < mPrevImgHeight; prevRow += mStride)
                {
                    for (int prevColumn = 0; prevColumn < mPrevImgWidth; prevColumn += mStride)
                    {
                        max = 0.0;
                        indexOfMax = channel * mCurImgHeight * mCurImgWidth + prevRow / mStride * mCurImgWidth + prevColumn / mStride;
                        for (int windowRow = 0; windowRow < mWindowHeight; windowRow++)
                        {
                            for (int windowColumn = 0; windowColumn < mWindowWidth; windowColumn++)
                            {
                                int curIndex = channel * mPrevImgHeight * mPrevImgWidth + (prevRow + windowRow) * mPrevImgWidth + (prevColumn + windowColumn);
                                if (inputs[curIndex] > max)
                                {
                                    max = inputs[curIndex];
                                    indexOfMax = curIndex;
                                }
                            }
                        }
                        int curOutputIndex = channel * mCurImgHeight * mCurImgWidth + prevRow / mStride * mCurImgWidth + prevColumn / mStride;
                        Totals[curOutputIndex] = indexOfMax;
                        Outputs[curOutputIndex] = max;
                    }
                }
            }
        }
    }
}
